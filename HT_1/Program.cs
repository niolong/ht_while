﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HT_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int currentNum, remPos=0;
            int a,value=0,i=0;

            Console.Write("Введите число: ");
            currentNum = int.Parse(Console.ReadLine());

            Console.Write("Введите искомое число: ");
            a = int.Parse(Console.ReadLine());

            while(currentNum!=0)
            {
                value = currentNum % 10;
                remPos = value;
                currentNum = currentNum / 10;
                
                if(remPos==a)
                {                    
                    i++;
                }
            }

            Console.WriteLine($"{a} в числе повторяется {i} раз");
            Console.ReadKey();
        }

    }
}
