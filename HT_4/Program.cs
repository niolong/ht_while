﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HT_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int a,b,i=0;
            int remA=0, remB=0;

            while (i < 15) 
            {
                Console.Write($"Введите {i} число: ");
                a = int.Parse(Console.ReadLine());
                               
                Console.Write($"Введите {i+1} число: ");
                b = int.Parse(Console.ReadLine());

                if (a == b && remA==0)
                {
                    remA = i;
                    remB = i + 1;
                }
             
                i+=2;
            }

            if (remA>0)
            {
                Console.WriteLine($"Пара находится на строке {remA} и {remB} ");
            }

            Console.ReadKey();           
        }
    }
}
