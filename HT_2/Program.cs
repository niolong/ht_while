﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HT_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            int value,sum=0;

            Console.Write("Введите число: ");
            num = int.Parse(Console.ReadLine());

            while(num!=0)
            {
                value = num % 10;
                num = num / 10;
                sum = value + sum;                
            }

            if (sum > 10)
            {
                Console.WriteLine("Yes");
           }
            else
            {
                Console.WriteLine("No");
            }

            Console.ReadKey();
        }
    }
}
