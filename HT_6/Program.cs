﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HT_6
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            int max=0,min = 10;
            int i=1;
            
            while(i<=10)
            {
                Console.Write($"Введите {i} число: ");
                num = int.Parse(Console.ReadLine());

                if(num>=max)
                {
                    max = num;                    
                }
                else if(num<=min)
                {
                    min = num;
                }
                i++;
            }

            Console.WriteLine($"Минимальное={min} Максимальное={max}");
            Console.ReadKey();
        }
    }
}
