﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HT_8
{
    class Program
    {
        static void Main(string[] args)
        {
            int i=1, day;
            int t;
            int savePos=0,tPos=0;

            Console.Write("Введите количество дней: ");
            day = int.Parse(Console.ReadLine());

            while (i<=day)
            {
                Console.Write("Введите температуру: ");
                t = int.Parse(Console.ReadLine());

                if (t > 0)
                {
                    savePos += 1;
                    tPos = savePos;
                    if(savePos>tPos)
                    {
                        tPos = savePos;
                    }
                }
                else
                {
                    savePos = 0;
                }                                                                

                i++;
            }

            if (tPos >= 2)
            {
                Console.WriteLine($"Дней засухи {tPos}");
            }
            else
            {
                Console.WriteLine("Засухи не было");
            }
            Console.ReadKey();
        }
    }
}
