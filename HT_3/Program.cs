﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HT_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            int value, i=0;

            Console.Write("Введите число: ");
            num = int.Parse(Console.ReadLine());

            while (num != 0)
            {
                value = num % 10;               
                num = num / 10;

                if(value==3)
                {
                    i++;
                }                                   
            }

            if (i > 0)
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }

            Console.ReadKey();
        }
    }
}
