﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HT_5
{
    class Program
    {
        static void Main(string[] args)
        {
            int num,value,lockMax=0,lockMin=9;
            int min=0, max=0;
            int razn;

            Console.Write("Введите число: ");
            num = int.Parse(Console.ReadLine());

            while(num!=0)
            {
                value = num % 10;
                num = num / 10;

                if(value>=lockMax)
                {
                    max = value;
                    lockMax = value + 1;
                }
                if(value<=lockMin)
                {
                   min = value;
                   lockMin=value-1;
                }
            }

            razn = max - min;

            if(razn%2==0)
            {
                Console.WriteLine("Является четным числом");
            }
            else
            {
                Console.WriteLine("Не является");
            }

            Console.WriteLine($"{max}=макс мин={min}");
            Console.ReadKey();
        }
    }
}
